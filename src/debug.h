
enum debuglvl { CRITICAL=0, WARNING=1, GENERAL=3, INFO=4, DEBUG=5 };
// CRITICAL: Critical application failure and has to shut down
// WARNING: A feature isn't working properly, but we got a workaround
// GENERAL: General applicative changes the user should experience
// INFO: Information the user isn't aware of happening in the application
// DEBUG: Useful for debugging programming problems, causes extreme logging

void log(const enum debuglvl lvl, const char *fmt, ...);
void debug_output_message(const char *fmt, ...);

void close_log();
void close_debug_messages();
