#ifndef __FILEHANDING_H
#define __FILEHANDING_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#define STRLEN 512
//#define GAMENAME "nether"
extern const char *GAMENAME;

enum filetype { GAMEDATA, USERDATA };
#ifndef _MODE_T_
#define	_MODE_T_
typedef unsigned short _mode_t;
#endif

int mkdirp(char *fqfn, _mode_t mode);
FILE *f1open(char *f, const char *m, const enum filetype t);
char *fixfilepath(char *srcpath, char *dstbuff, const int buffsize);
#endif
