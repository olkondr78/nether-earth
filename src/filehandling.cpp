#include "filehandling.h"
#ifdef _WIN32
#include "direct.h"
#endif

const char *GAMENAME = "nether";

// make (sub)directories including multiple subdirs
int mkdirp(char *fqfn, _mode_t mode) {
  char *t, str[STRLEN];
  struct stat stbuf;
  int len;
  bool prevsep = true;
  const char exseparator =
#ifdef _WIN32
          '\\';
#else
          0;
#endif

  t=(char *)fqfn;
  memset(str, '\0', STRLEN);

  while (*t) {
    if (*t == '/' || (exseparator != 0 && *t == exseparator)) {
      len = t-fqfn;
      if ((len < STRLEN) && (len > 0) && !prevsep && (len != 2 || fqfn[1] != ':')) {
        strncpy(str, fqfn, len);
        if (stat(str, &stbuf) != 0) {
          int r;
#ifdef _WIN32
          r = _mkdir(str);
#else
          r = mkdir(str, mode);
#endif
          if (r != 0) return(-1);
        }
      }
      prevsep = true;
    } else {
      prevsep = false;
    }
    t++;
  }

  return(0);
}

// new fopen()
FILE *f1open(char *f, const char *m, const enum filetype t) {
  char fname[STRLEN];
  char *r;

  switch (t) {
    case GAMEDATA:
      // gamedata is read-only
#ifdef _WIN32
      r = fixfilepath(f, fname, STRLEN);
#else
      r = f;
#endif
      return(fopen(r, m));
    case USERDATA:
      // userdata is put in $HOME/.GAMENAME/
      char *home;
#ifdef _WIN32
      home = getenv("APPDATA");
#else
      home = getenv("HOME");
#endif
      if (home != NULL) {
#ifdef _WIN32
        snprintf(fname, STRLEN-1, "%s\\%s\\%s", home, GAMENAME, f);
#else
        snprintf(fname, STRLEN-1, "%s/.%s/%s", home, GAMENAME, f);
#endif
        // create subdirs if they don't exist
        mkdirp(fname, S_IRWXU | S_IRGRP|S_IXGRP | S_IROTH|S_IXOTH);
        r = fixfilepath(fname, fname, STRLEN);
      } else {
        r = fixfilepath(f, fname, STRLEN);
      }
      // open file
      return(fopen(r, m));
  }
  // should not be reached
  return(NULL);
}

char *fixfilepath(char *srcpath, char *dstbuff, const int buffsize)
{
#ifdef _WIN32
    char *p = (char *)srcpath, *d = dstbuff;
    int len = 0;
    const int minsize = buffsize-1;
    while(*p && len < minsize) {
        if (*p == '/') {
            *d = '\\';
        } else {
            *d = *p;
        }
        ++d;
        ++p;
        ++len;
    }
    *d = 0;
    return dstbuff;
#else
    return srcpath;
#endif
}


