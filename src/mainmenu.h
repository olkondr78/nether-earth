#ifndef MAINMENU_H
#define MAINMENU_H

enum MainMenuStatus {
    mmsAnimTitle = 0,
    mmsTitle = 1,
    mmsAnimOptions = 2,
    mmsOptions = 3,
    mmsStartNewGame = 4,
    mmsExitGame = 5,
    mmsAnimKeyb = 6,
    mmsKeyb = 7,
    mmsAnimHelp = 8,
    mmsHelp = 9
};

#endif // MAINMENU_H
