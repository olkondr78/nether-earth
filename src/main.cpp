#ifdef _WIN32
#include <windows.h>
#include <windowsx.h>
#else
#include <sys/time.h>
#include <time.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include "SDL.h"
#include "SDL_mixer.h"
#include <glut.h>

#include "List.h"
#include "vector.h"

#include "cmc.h"
#include "3dobject.h"
#include "shadow3dobject.h"
#include "piece3dobject.h"
#include "nether.h"
#include "mainmenu.h"

#include "debug.h"

/*						GLOBAL VARIABLES INITIALIZATION:							*/ 

extern debuglvl dbglvl;

int SCREEN_X=640;
int SCREEN_Y=480;

int COLOUR_DEPTH=16;
int shadows=1;
int detaillevel=4;
bool sound=true;
int up_key=SDLK_UP,down_key=SDLK_DOWN,left_key=SDLK_LEFT,right_key=SDLK_RIGHT,fire_key=SDLK_SPACE,pause_key=SDLK_ESCAPE;
int level=1;
MainMenuStatus mainmenu_status=mmsAnimTitle;
int mainmenu_substatus=0;
bool fullscreen=false;
bool show_radar=true;
char mapname[128]="original.map";
C3DObject *nethertittle=0;

/* DRAWING REGION AROUND THE SHIP: */ 
float MINY=-8,MAXY=8,MINX=-8,MAXX=8;

/* Redrawing constant: */ 
const int REDRAWING_PERIOD=20;

/* Frames per second counter: */ 
int frames_per_sec=0;
int frames_per_sec_tmp=0;
int init_time=0;


/* Surfaces: */ 
SDL_Surface *screen_sfc;

NETHER *game=0;

void save_configuration(void);
void load_configuration(void);
int mainmenu_cycle(int width,int height);
void mainmenu_draw(int width,int height);

/*						AUXILIAR FUNCTION DEFINITION:							*/ 


void pause(unsigned int time)
{
	unsigned int initt=SDL_GetTicks();

	while((SDL_GetTicks()-initt)<time);
} /* pause */ 


SDL_Surface *initialization(int flags) 
{
    const SDL_VideoInfo* info=0;
    int bpp=0;
	SDL_Surface *screen;

    debug_output_message("SDL_Init()");

    if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO)<0) {
        char * err = SDL_GetError();
        debug_output_message("Video initialization failed: %s\n",err);
        //fprintf(stderr,"Video initialization failed: %s\n",SDL_GetError());
        return NULL;
    } /* if */ 

    debug_output_message("SDL_GetVideoInfo()");
    info=SDL_GetVideoInfo();

    if(!info) {
        char * err = SDL_GetError();
        debug_output_message("Video query failed: %s\n",err);
        //fprintf(stderr,"Video query failed: %s\n",err);
        return NULL;
    } /* if */ 

	if (fullscreen) {
		bpp=COLOUR_DEPTH;
	} else {
	    bpp=info->vfmt->BitsPerPixel;
	} /* if */ 

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE,1);

    flags=SDL_OPENGL|flags;

	screen=SDL_SetVideoMode(SCREEN_X,SCREEN_Y,bpp,flags);
    if(screen==0) {
        char * err = SDL_GetError();
        debug_output_message("Video mode set failed: %s\n",err);
        //fprintf(stderr,"Video mode set failed: %s\n",err);
        return NULL;
    } /* if */ 

//	glutInitDisplayMode(GLUT_DEPTH | GLUT_SINGLE | GLUT_RGBA);
//	glutInitWindowPosition(100,100);
//	glutInitWindowSize(SCREEN_X,SCREEN_Y);

    debug_output_message("pause(400)");
    pause(400);
    debug_output_message("Mix_OpenAudio()");
    if (Mix_OpenAudio(22050, AUDIO_S16, 2, 1024)) {
        return NULL;
	} /* if */ 

    debug_output_message("SDL_WM_SetCaption()");
    SDL_WM_SetCaption(GAMECAPTION, 0);
	SDL_ShowCursor(SDL_DISABLE);

	return screen;
} /* initialization */ 


void finalization()
{
	Mix_CloseAudio();
	SDL_Quit();
} /* finalization */ 


SDL_Surface *toogle_video_mode(bool fullscreen)
{
	SDL_Surface *sfc = 0;
	if (game!=0) game->refresh_display_lists();
	if (nethertittle!=0) nethertittle->refresh_display_lists();
	if (game!=0) game->deleteobjects();
		
	SDL_QuitSubSystem(SDL_INIT_VIDEO);
	SDL_InitSubSystem(SDL_INIT_VIDEO);
	
	sfc = SDL_SetVideoMode(SCREEN_X, SCREEN_Y, COLOUR_DEPTH, SDL_OPENGL|(fullscreen ? SDL_FULLSCREEN : 0));

	if (game!=0) game->loadobjects();
	
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE,8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,16);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE,1);
	
    SDL_WM_SetCaption(GAMECAPTION, 0);
	SDL_ShowCursor(SDL_DISABLE);
	return sfc;
} /* toogle_video_mode */




#ifdef _WIN32
int PASCAL WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance,
                    LPSTR lpCmdLine, int nCmdShow)
{
   int argc = __argc;
   char ** argv = __argv;
#else
int main(int argc, char** argv)
{
#endif

	int time,act_time;
	SDL_Event event;
    bool quit = false;

	dbglvl = WARNING;

    debug_output_message("load_configuration()");

	load_configuration();
	
    debug_output_message("glutInit()");

    glutInit(&argc, argv);

    debug_output_message("initialization()");

    screen_sfc = initialization((fullscreen ? SDL_FULLSCREEN : 0));
	if (screen_sfc==0) return 0;

    debug_output_message("SDL_GetTicks()");
    time=init_time=SDL_GetTicks();

	while (!quit) {
		while( SDL_PollEvent( &event ) ) {
            switch( event.type ) {
                /* Keyboard event */
                case SDL_KEYDOWN:
#ifdef __APPLE__
                    // different quit shortcut on OSX: apple+Q
                    if (event.key.keysym.sym == SDLK_q) {
                        SDLMod modifiers;
                        modifiers = SDL_GetModState();
                        if ((modifiers&KMOD_META) != 0) {
                            quit = true;
                        }
                    }
#endif
#ifdef _WIN32
                    // different quit shortcut on WIN32: ALT+F4
                    if (event.key.keysym.sym == SDLK_F4) {
                        SDLMod modifiers;
                        modifiers = SDL_GetModState();
                        if ((modifiers&KMOD_ALT) != 0) {
                            quit = true;
                        }
                    }
#endif
                    // default quit: F12
                    if (event.key.keysym.sym == SDLK_F12) {
                        quit = true;
                    } /* if */
					
#ifdef __APPLE__
                    if (event.key.keysym.sym == SDLK_f) {
                        SDLMod modifiers;
                        modifiers = SDL_GetModState();
                        if ((modifiers&KMOD_META) != 0) {
                            fullscreen = (fullscreen ? false : true);
                            screen_sfc = toogle_video_mode(fullscreen);
//							if (screen_sfc==0) quit = true;
                        } // if
                    } // if
#else
                    if (event.key.keysym.sym == SDLK_RETURN)
                    {
                        SDLMod modifiers;
                        modifiers = SDL_GetModState();
                        if ((modifiers&KMOD_ALT) != 0) {
                            fullscreen = (fullscreen ? false : true);
                            screen_sfc = toogle_video_mode(fullscreen);
//							if (screen_sfc==0) quit = true;
                        } // if
                    } // if
#endif
                    break;

                /* SDL_QUIT event (window close) */
                case SDL_QUIT:
                    quit = true;
                    break;
            } /* switch */ 
        } /* while */ 

		act_time=SDL_GetTicks();
		if (act_time-time>=REDRAWING_PERIOD)
		{
			frames_per_sec_tmp+=1;
			if ((act_time-init_time)>=1000) {
				frames_per_sec=frames_per_sec_tmp;
				frames_per_sec_tmp=0;
				init_time=act_time;
			} /* if */ 

			do {
				time+=REDRAWING_PERIOD;
				if ((act_time-time)>50*REDRAWING_PERIOD) time=act_time;
			
				if (game!=0) {
					if (!game->gamecycle(SCREEN_X,SCREEN_Y)) {
                        debug_output_message("delete game");
						delete game;
						game=0;
                        mainmenu_status=mmsAnimTitle;
						mainmenu_substatus=0;
					} /* if */  
				} else {
					int val=mainmenu_cycle(SCREEN_X,SCREEN_Y);
					if (val==1) {
                        debug_output_message("new NETHER(%s)",mapname);
						char tmp[256];
						sprintf(tmp,"maps/%s",mapname);
						game=new NETHER(tmp);
					} /* if */ 
                    if (val==2) {
                        debug_output_message("quit, val=2");
                        quit=true;
                    }
					if (val==3) {
                        if (game!=NULL) {
                            debug_output_message("game->refresh_display_lists()");
                            game->refresh_display_lists();
                        }
                        if (nethertittle!=NULL) {
                            debug_output_message("nethertittle->refresh_display_lists()");
                            nethertittle->refresh_display_lists();
                        }
                        if (game!=NULL) {
                            debug_output_message("game->deleteobjects()");
                            game->deleteobjects();
                        }
                        debug_output_message("SDL_QuitSubSystem()");
                        SDL_QuitSubSystem(SDL_INIT_VIDEO);
                        debug_output_message("SDL_InitSubSystem()");
                        SDL_InitSubSystem(SDL_INIT_VIDEO);
                        debug_output_message("check SDL_WasInit()");
                        if (SDL_WasInit(SDL_INIT_VIDEO)) {

                            debug_output_message("SDL_GL_SetAttribute()");
							SDL_GL_SetAttribute(SDL_GL_RED_SIZE,8);
							SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,8);
							SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,8);
							SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,16);
							SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);
							SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE,1);

                            debug_output_message("SDL_SetVideoMode()");
                            screen_sfc = SDL_SetVideoMode(SCREEN_X, SCREEN_Y, COLOUR_DEPTH, SDL_OPENGL|(fullscreen ? SDL_FULLSCREEN : 0));
                            if (game!=0) {
                                debug_output_message("game->loadobjects()");
                                game->loadobjects();
                            }
                            debug_output_message("SDL_WM_SetCaption()");
                            SDL_WM_SetCaption(GAMECAPTION, 0);
							SDL_ShowCursor(SDL_DISABLE);
						} else {
                            debug_output_message("quit, not SDL_WasInit()");
                            quit = true;
						} /* if */ 
					} /* if */ 
				} /* if */ 
				act_time=SDL_GetTicks();
			} while(act_time-time>=REDRAWING_PERIOD);

			if (game!=0) {
				game->gameredraw(SCREEN_X,SCREEN_Y);
			} else {
				mainmenu_draw(SCREEN_X,SCREEN_Y);
			} /* if */ 
		} /* if */ 
		SDL_Delay(REDRAWING_PERIOD-(act_time-time));
	} /* while */ 

	delete game;

	finalization();

    close_log();

	return 0;
}










