project(nether-earth)
cmake_minimum_required(VERSION 3.5)
# project(nether-earth LANGUAGES CXX)

# set(CMAKE_CXX_STANDARD 17)
# set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(PkgConfig REQUIRED)
find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)
find_package(SDL REQUIRED)
find_package(SDL_mixer REQUIRED)

if(NOT ${OPENGL_FOUND})
    message("OPENGL not found")
endif()

# message("CMAKE_ROOT: '${CMAKE_ROOT}', TOOLCHAIN_ROOT: '${TOOLCHAIN_ROOT}', SDL_INCLUDE_DIR: '${SDL_INCLUDE_DIR}'")
# message("SDL_INCLUDE_DIR: '${SDL_INCLUDE_DIR}'")
# message("OPENGL_INCLUDE_DIR: '${OPENGL_INCLUDE_DIR}', OPENGL_LIBRARY: '${OPENGL_LIBRARY}'")

if(EXISTS ${OPENGL_INCLUDE_DIR})
    message("found OPENGL_INCLUDE_DIR: '${OPENGL_INCLUDE_DIR}'")
else()
    unset(OPENGL_INCLUDE_DIR CACHE)
    find_path(OPENGL_INCLUDE_DIR NAMES GL/gl.h
        PATHS /usr/include /include ${CMAKE_ROOT}/../../include
    )
    message("create OPENGL_INCLUDE_DIR: '${OPENGL_INCLUDE_DIR}'")
endif()

if(EXISTS ${GLUT_INCLUDE_DIRS})
    message("found GLUT_INCLUDE_DIRS: '${GLUT_INCLUDE_DIRS}'")
else()
    unset(GLUT_INCLUDE_DIRS CACHE)
    find_path(GLUT_INCLUDE_DIRS NAMES freeglut.h
        PATHS /usr/include/GL /include/GL ${CMAKE_ROOT}/../../include/GL
    )
    message("create GLUT_INCLUDE_DIRS: '${GLUT_INCLUDE_DIRS}'")
endif()

# add_subdirectory(src)
include_directories(src)
include_directories(${OPENGL_INCLUDE_DIR}/GL)
include_directories(${GLUT_INCLUDE_DIRS})
include_directories(${SDL_INCLUDE_DIR})
# target_include_directories(nether-earth src)

#get_property(dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES)
#foreach(dir ${dirs})
#  message(STATUS "dir='${dir}'")
#endforeach()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2 -Wall -Wextra -std=c++11")

add_executable(nether-earth
    src/3dobject-ase.cpp
    src/3dobject.cpp
    src/bitmap.cpp
    src/bullet.cpp
    src/cmc.cpp
    src/construction.cpp
    src/enemy_ai.cpp
    src/glprintf.cpp
    src/main.cpp
    src/mainmenu.cpp
    src/maps.cpp
    src/menu.cpp
    src/myglutaux.cpp
    src/nether.cpp
    src/nethercycle.cpp
    src/netherdebug.cpp
    src/nethersave.cpp
    src/particles.cpp
    src/piece3dobject.cpp
    src/quaternion.cpp
    src/radar.cpp
    src/robot_ai.cpp
    src/robots.cpp
    src/shadow3dobject.cpp
    src/vector.cpp
    src/debug.cpp
    src/filehandling.cpp
)

# target_include_directories(nether-earth PRIVATE ${CONFIG_INCLUDE_DIR})

target_link_libraries(nether-earth
   ${OPENGL_LIBRARY}
   ${GLUT_LIBRARIES}
   ${OPENGL_glu_LIBRARY}
   ${GLUT_glut_LIBRARY}
   ${SDL_LIBRARY}
   ${SDLMIXER_LIBRARY}
)

set_target_properties(nether-earth PROPERTIES
  RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
  LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
  ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)
